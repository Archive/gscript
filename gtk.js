/* Simulate a GIO-style async operation using idles */

function open_async(filename, flags, cb) {
    function on_idle() {
	var res = new Object();
	if (filename == "test.txt")
	    res.data = "Test data";
	else if (filename == "test2.txt")
	    res.data = "Test2 data";
	else
	    res.data = "other data";
	cb (filename, res);
	
    }
    add_idle (on_idle);
}

function open_finish(filename, res) {
    return res.data;
}

/* Define AsyncRunner class */

function AsyncRunner(_fn) {
    this.fn = _fn;
    this.fn.__asyncrunner = true
}

AsyncRunner.prototype.toString = function(cb) {
    return "[AsyncRunner]";
}

/* static function to help wrapping async functions */
AsyncRunner.curry_function = function (fn, fn_finish) {
    /* We return a curried version of foo_async with everything but the
     * callback argument bound. Plus we pass the (curried) paired finish method as a
     * property of the return value function.
     */
    let curried_args = arguments;
    let curried = function (cb) {
	let args = []
	for (let i = 2; i < curried_args.length; i++)
	    args.push (curried_args[i]);
	args.push(cb);
	fn.apply (this, args);
	
    }
    curried.finish = function(res) {
	return open_finish (curried_args[0], res);
    }
    return curried;
}

/* This function lets us run a generator function as an async operation,
   handling all the callbacks from the yields. You can also pass in
   a callback to run when the operation is done and a list of arguments
   for the generator */

AsyncRunner.prototype.start = function (cb /* , args ... */) {
    function async_cb (obj, res) {
	let ret;
	try {
	    ret = f.finish (res);
	    f = gen.send(ret);
	} catch (e if e instanceof StopIteration) {
	    f = undefined;
	} catch (e) {
	    f = gen.throw(e);
	}
	if (f != null && f instanceof Function && "finish" in f)
	    f (async_cb);
	else if (cb)
	    cb (f);
    }
    let extra_args = [];
    for (let i = 1; i < arguments.length; i++)
	extra_args[i-1] = arguments[i];
    let gen = this.fn.apply (this, extra_args);
    let f = gen.next();
    if (f != null && f instanceof Function && "finish" in f)
	f (async_cb);
    else
	cb(f);
}

/* Lets you call other async generators from an async generator */
AsyncRunner.prototype.call = function (/* args... */) {

    /* Verify we're not being called wrongly */
    let caller = arguments.callee.caller;
    if ((caller == null) ||
	!(caller instanceof Function) ||
	!("__asyncrunner" in caller)) {
	throw new Error ("Can only call AsyncRunner.call() from AsyncRunner function");
    }
    
    var args = [null];
    var runner = this;
    for (let i = 0; i < arguments.length; i++)
	args.push(arguments[i]);
    
    let f = function (cb) {
	function wrap_cb (res) {
	    cb (null, res);
	}
	args[0] = wrap_cb;
	runner.start.apply (runner, args);
    }
    f.finish = function (res) {
	return res;
    }
    return f;
}

/* Wrap the GIO-style async op for the AsyncRunner yield based format */

function $open (filename, flags) {
    return AsyncRunner.curry_function (open_async, open_finish, filename, flags);
}
    
/* Test this stuff */

do_stuff2 = new AsyncRunner (function (filename) {
	var data3 = yield $open (filename, 0);
	print ("data for " + filename + " = " + data3);
	yield "the end (of do_stuff2)"
    });

do_stuff = new AsyncRunner (function  (filenames) {
	for (let i in filenames) {
	    var data = yield $open (filenames[i], 0);
	    print ("data for " + filenames[i] + "  = " + data);
	}
	var ret = yield do_stuff2.call("other.txt");
	print ("AsyncRunner call returned: " + ret);
    });

do_stuff.start(null, ["test.txt", "test2.txt"]);
