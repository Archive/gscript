#include "gscriptvalue.h"
#include "gscriptinternal.h"

/**
 * GScriptValue:
 * 
 */
struct _GScriptValue
{
  GObject parent_instance;

  jsval value;
};

G_DEFINE_TYPE (GScriptValue, g_script_value, G_TYPE_OBJECT);

static void
g_script_value_finalize (GObject *object)
{
  GScriptValue *value;
  JSContext *cx;

  value = G_SCRIPT_VALUE (object);

  cx = _g_script_get_internal_context ();
  
  JS_RemoveRoot (cx, &value->value);

  G_OBJECT_CLASS (g_script_value_parent_class)->finalize (object);
}

static void
g_script_value_class_init (GScriptValueClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = g_script_value_finalize;
}

static void
g_script_value_init (GScriptValue *value)
{
  JSContext *cx;
  gboolean ok;

  cx = _g_script_get_internal_context ();

  value->value = JSVAL_VOID;

  ok = JS_AddNamedRoot (cx, &value->value, "GScriptValue");

  if (!ok)
    {
      /* TODO: What to do here? make the object not working? */
    }
}

jsval
_g_script_value_get_jsval (GScriptValue *value)
{
  return value->value;
}

/**
 * g_script_value_new:
 * 
 * Creates a new script value of type undefined
 *
 * Returns: a #GScriptValue.
 **/
GScriptValue *
g_script_value_new (void)
{
  JSContext *cx = _g_script_get_internal_context ();
  GScriptValue *value;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  value->value = OBJECT_TO_JSVAL (JS_NewObject (cx, NULL, NULL, NULL));
  
  return value;
}

GScriptValue *
g_script_value_new_undefined (void)
{
  return g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
}

GScriptValue *
_g_script_value_new_from_jsval (jsval js_value)
{
  GScriptValue *value;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
  value->value = js_value;
  return value;
}

GScriptValue *
_g_script_value_new_from_jsobject (JSObject *obj)
{
  return _g_script_value_new_from_jsval (OBJECT_TO_JSVAL (obj));
}


GScriptValue  *
g_script_value_new_from_gvalue (GValue *gvalue)
{
  GScriptValue *value;

  if (!G_IS_VALUE (gvalue))
    return g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
  
  switch (G_VALUE_TYPE (gvalue))
    {
    case G_TYPE_BOOLEAN:
      value = g_script_value_new_from_boolean (g_value_get_boolean (gvalue));
      break;

    case G_TYPE_CHAR:
      value = g_script_value_new_from_int32 (g_value_get_char (gvalue));
      break;
      
    case G_TYPE_UCHAR:
      value = g_script_value_new_from_uint32 (g_value_get_uchar (gvalue));
      break;
      
    case G_TYPE_INT:
      value = g_script_value_new_from_int32 (g_value_get_int (gvalue));
      break;
      
    case G_TYPE_UINT:
      value = g_script_value_new_from_uint32 (g_value_get_uint (gvalue));
      break;
      
    case G_TYPE_LONG:
      value = g_script_value_new_from_double ((double)g_value_get_long (gvalue));
      break;
      
    case G_TYPE_ULONG:
      value = g_script_value_new_from_double ((double)g_value_get_ulong (gvalue));
      break;
      
    case G_TYPE_INT64:
      value = g_script_value_new_from_double ((double)g_value_get_int64 (gvalue));
      break;
      
    case G_TYPE_UINT64:
      value = g_script_value_new_from_double ((double)g_value_get_uint64 (gvalue));
      break;
      
    case G_TYPE_FLOAT:
      value = g_script_value_new_from_double ((double)g_value_get_float (gvalue));
      break;
      
    case G_TYPE_DOUBLE:
      value = g_script_value_new_from_double ((double)g_value_get_double (gvalue));
      break;
      
    case G_TYPE_STRING:
      value = g_script_value_new_from_string (g_value_get_string (gvalue));
      break;

    default:
      value = NULL;
    }

  if (value == NULL)
    {
      if (g_type_is_a (G_VALUE_TYPE (gvalue),G_TYPE_ENUM))
	value = g_script_value_new_from_double ((double)gvalue->data[0].v_long);
      else if (g_type_is_a (G_VALUE_TYPE (gvalue),G_TYPE_FLAGS))
	value = g_script_value_new_from_double ((double)gvalue->data[0].v_ulong);
      else if (g_type_is_a (G_VALUE_TYPE (gvalue),G_TYPE_OBJECT))
	{
	  JSContext *cx = _g_script_get_internal_context ();
	  JSObject *js_obj;
	  GObject *gobject;
	  
	  /* TODO: This creates the classes in the internal contexts global,
	     not in the callers global. Does this matter? */
	  
	  js_obj = JSVAL_NULL;
	  if (JS_AddRoot (cx, &js_obj))
	    {
	      gobject = g_value_get_object (gvalue);
	      if (gobject == NULL)
		{
		  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
		  value->value = JSVAL_NULL;
		}
	      else if (wrap_object (gobject, &js_obj))
		{
		  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
		  value->value = OBJECT_TO_JSVAL(js_obj);
		}
	      JS_RemoveRoot (cx, &js_obj);
	    }
	}
    }
      
  return value; /* can be NULL on failed convert */
}

GScriptValue *
g_script_value_new_from_double (double v)
{
  GScriptValue *value;
  JSContext *cx = _g_script_get_internal_context ();

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  if (!JS_NewNumberValue (cx, (double)v, &value->value))
    value->value = JSVAL_VOID;
  
  return value;
}

GScriptValue *
g_script_value_new_from_boolean (gboolean v)
{
  GScriptValue *value;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  value->value = BOOLEAN_TO_JSVAL (v);
  
  return value;
}


GScriptValue *
g_script_value_new_from_int32 (gint32 v)
{
  GScriptValue *value;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  if (INT_FITS_IN_JSVAL (v))
    value->value = INT_TO_JSVAL (v);
  else
    {
      JSContext *cx = _g_script_get_internal_context ();

      if (!JS_NewNumberValue (cx, (double)v, &value->value))
	value->value = JSVAL_VOID;
    }
  
  return value;
}

GScriptValue *
g_script_value_new_from_uint32 (guint32 v)
{
  GScriptValue *value;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  if (((gint32)v) >= 0 &&
      INT_FITS_IN_JSVAL ((gint32)v))
    value->value = INT_TO_JSVAL ((gint32)v);
  else
    {
      JSContext *cx = _g_script_get_internal_context ();

      if (!JS_NewNumberValue (cx, (double)v, &value->value))
	value->value = JSVAL_VOID;
    }
  
  return value;
}

GScriptValue *
g_script_value_new_from_string (const char *str)
{
  JSContext *cx = _g_script_get_internal_context ();
  GScriptValue *value;
  jschar *str2;
  JSString *js_str;

  value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
  str2 = g_utf8_to_utf16 (str, -1, NULL, NULL, NULL);
  if (str2)
    {
      js_str = JS_NewUCStringCopyZ (cx, (const jschar *)str2);
      value->value = STRING_TO_JSVAL(js_str);
      g_free (str2);
    }
      
  return value;
}

GScriptValue *
g_script_value_new_from_gobject (GObject *object)
{
  GScriptValue *value;
  JSObject *js_object;

  value = NULL;
  
  js_object = NULL;
  if (wrap_object (object, &js_object))
    {
      value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
      value->value = OBJECT_TO_JSVAL (js_object);
    }
  else
    g_warning ("Couldn't wrap object");
  
  return value;
}

static JSBool
call_native_function (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
  GScriptValue *ret;
  GScriptValue **gargs;
  GScriptNativeFunction native, *nativep;
  jsval v;
  int i;
  
  if (!JS_GetReservedSlot (cx, JS_GetFunctionObject (JS_ValueToFunction (cx, argv[-2])), 0, &v))
    return JS_FALSE;

  nativep = JSVAL_TO_PRIVATE (v);
  native = *nativep;

  gargs = g_new (GScriptValue *, argc);
  for (i = 0; i < argc; i++)
    gargs[i] = _g_script_value_new_from_jsval (argv[i]);
  
  ret = native (argc, gargs);

  if (ret)
    {
      *rval = ret->value;
      g_object_unref (ret);
    }
  else
    *rval = JSVAL_VOID;

  for (i = 0; i < argc; i++)
    g_object_unref (gargs[i]);
  
  g_free (gargs);

  return JS_TRUE;
}

GScriptValue *
g_script_value_new_from_function (GScriptNativeFunction function, int n_args)
{
  GScriptValue *new_value;
  JSContext *cx = _g_script_get_internal_context ();
  JSFunction *func;
  JSObject *object;
  GScriptNativeFunction *nativep;
  
  new_value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
  func = JS_NewFunction (cx, call_native_function, n_args, 0, NULL, NULL);

  object = JS_GetFunctionObject (func);
  new_value->value = OBJECT_TO_JSVAL (object);

  nativep = g_new (GScriptNativeFunction, 1);
  g_assert (((gsize)nativep) % 2 == 0);
  *nativep = function;

  if (!JS_SetReservedSlot (cx, object, 0, PRIVATE_TO_JSVAL (nativep)))
    g_warning ("Unable to set reserved slot of function");
  
  return new_value;
}

GScriptValue *
g_script_value_dup (GScriptValue *value)
{
  GScriptValue *new_value;

  new_value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  new_value->value = value->value;
  
  return new_value;
}

gboolean
g_script_value_is_null (GScriptValue *value)
{
  return JSVAL_IS_NULL (value->value);
}

gboolean
g_script_value_is_undefined (GScriptValue *value)
{
  return JSVAL_IS_VOID (value->value);
}

gboolean
g_script_value_is_boolean (GScriptValue *value)
{
  return JSVAL_IS_BOOLEAN (value->value);
}

gboolean
g_script_value_is_number (GScriptValue *value)
{
  return JSVAL_IS_NUMBER (value->value);
}

gboolean
g_script_value_is_string (GScriptValue *value)
{
  return JSVAL_IS_STRING (value->value);
}

gboolean
g_script_value_is_object (GScriptValue *value)
{
  return
    JSVAL_IS_OBJECT (value->value) &&
    value->value != JSVAL_NULL;
}

gboolean
g_script_value_is_function (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  return
    JSVAL_IS_OBJECT (value->value) &&
    value->value != JSVAL_NULL &&
    JS_ObjectIsFunction(cx, JSVAL_TO_OBJECT (value->value));
}

gboolean
g_script_value_is_gobject (GScriptValue *value)
{
  GObject *gobject;
  
  if (!JSVAL_IS_OBJECT (value->value) ||
      value->value == JSVAL_NULL)
    return FALSE;

  gobject = _js_object_get_gobject (JSVAL_TO_OBJECT (value->value));
  
  return gobject != NULL;
}

gboolean
g_script_value_to_boolean (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  JSBool v;

  if (JS_ValueToBoolean (cx, value->value, &v))
    return v;
  return FALSE;
}

gint32
g_script_value_to_int32 (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  gint32 v;

  if (JS_ValueToECMAInt32 (cx, value->value, &v))
    return v;
  return 0;
}

guint32
g_script_value_to_uint32 (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  guint32 v;

  if (JS_ValueToECMAUint32 (cx, value->value, &v))
    return v;
  return 0;
}

double
g_script_value_to_double (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  jsdouble v;

  if (JS_ValueToNumber (cx, value->value, &v))
    return v;
  return 0;
}

char *
g_script_value_to_string (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  JSString *str;
  jschar *chars;
  size_t len;
  char *res;

  str = JS_ValueToString (cx, value->value);

  if (str == NULL)
    return NULL;
  
  chars = JS_GetStringChars (str);
  len = JS_GetStringLength (str);
  
  res = g_utf16_to_utf8 (chars, len, NULL, NULL, NULL);
  return res;
}

GScriptValue *
g_script_value_to_object (GScriptValue *value)
{
  JSContext *cx = _g_script_get_internal_context ();
  GScriptValue *new_value;
  JSObject *object;

  new_value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);

  if (!JS_ValueToObject (cx, value->value, &object))
    new_value->value = OBJECT_TO_JSVAL(object);

  return new_value;
}

GObject *
g_script_value_to_gobject (GScriptValue *value)
{
  GObject *gobject;
  
  if (!JSVAL_IS_OBJECT (value->value) ||
      value->value == JSVAL_NULL)
    return FALSE;

  gobject = _js_object_get_gobject (JSVAL_TO_OBJECT (value->value));
  
  return gobject;
}

gboolean
g_script_value_to_gvalue (GScriptValue *value,
			  GType enforce_type,
			  GValue *gvalue)
{
  switch (enforce_type)
    {
    case G_TYPE_BOOLEAN:
      {
	gboolean v;
	
	if (!g_script_value_is_boolean (value))
	  return FALSE;
	
	g_value_init (gvalue, G_TYPE_BOOLEAN);
	v = g_script_value_to_boolean (value);
	g_value_set_boolean (gvalue, v);
	return TRUE;
      }
    case G_TYPE_INT:
      {
	gint32 v;
	
	if (!g_script_value_is_number (value))
	  return FALSE;
	
	g_value_init (gvalue, G_TYPE_INT);
	v = g_script_value_to_int32 (value);
	g_value_set_int (gvalue, v);
	return TRUE;
      }
    case G_TYPE_UINT:
      {
	guint32 v;
	
	if (!g_script_value_is_number (value))
	  return FALSE;
	
	g_value_init (gvalue, G_TYPE_INT);
	v = g_script_value_to_int32 (value);
	g_value_set_int (gvalue, v);
	return TRUE;
      }
    case G_TYPE_CHAR:
      {
	gint32 v;
	
	if (!g_script_value_is_number (value))
	  return FALSE;

	g_value_init (gvalue, G_TYPE_INT);
	v = g_script_value_to_int32 (value);

	if (v < G_MININT8 || v > G_MAXINT8)
	  return FALSE;
	
	g_value_set_char (gvalue, v);
	return TRUE;
      }
    case G_TYPE_UCHAR:
      {
	gint32 v;
	
	if (!g_script_value_is_number (value))
	  return FALSE;

	g_value_init (gvalue, G_TYPE_INT);
	v = g_script_value_to_int32 (value);

	if (v < 0 || v > G_MAXUINT8)
	  return FALSE;
	
	g_value_set_uchar (gvalue, v);
	return TRUE;
      }
    case G_TYPE_LONG:
    case G_TYPE_ULONG:
    case G_TYPE_INT64:
    case G_TYPE_UINT64:
    case G_TYPE_FLOAT:
    case G_TYPE_DOUBLE:
      {
	double v;
	
	if (!g_script_value_is_number (value))
	  return FALSE;
	
	v = g_script_value_to_double (value);
	
	g_value_init (gvalue, enforce_type);

	/* TODO: Range checking */
	
	switch (enforce_type)
	  {
	  case G_TYPE_LONG:
	    g_value_set_long (gvalue, (long)v);
	    break;
	  case G_TYPE_ULONG:
	    g_value_set_ulong (gvalue, (ulong)v);
	    break;
	  case G_TYPE_INT64:
	    g_value_set_int64 (gvalue, (gint64)v);
	    break;
	  case G_TYPE_UINT64:
	    g_value_set_uint64 (gvalue, (guint64)v);
	    break;
	  case G_TYPE_FLOAT:
	    g_value_set_float (gvalue, v);
	    break;
	  case G_TYPE_DOUBLE:
	    g_value_set_double (gvalue, v);
	    break;
	  }
		
	return TRUE;
      }
    case G_TYPE_STRING:
      {
	char *s;
	
	if (!g_script_value_is_string (value))
	  return FALSE;
	
	s = g_script_value_to_string (value);
	g_value_init (gvalue, enforce_type);
	g_value_take_string (gvalue, s);
		
	return TRUE;
      }
    default:
      
      break;
    }

  if (g_type_is_a (enforce_type, G_TYPE_ENUM) &&
      g_script_value_is_number (value))
    {
      gint32 v;

      v = g_script_value_to_int32 (value);
      g_value_init (gvalue, enforce_type);
      gvalue->data[0].v_long = v;
      return TRUE;
    }
  else if (g_type_is_a (enforce_type, G_TYPE_FLAGS) &&
      g_script_value_is_number (value))
    {
      guint32 v;

      v = g_script_value_to_uint32 (value);
      g_value_init (gvalue, enforce_type);
      gvalue->data[0].v_ulong = v;
      return TRUE;
    }
  else if (g_type_is_a (enforce_type, G_TYPE_OBJECT) &&
	   (g_script_value_is_null (value) ||
	    g_script_value_is_gobject (value)))
    {
      GObject *o;

      o = g_script_value_to_gobject (value);

      if (o == NULL ||
	  g_type_is_a (G_OBJECT_TYPE (o), enforce_type))
	{
	  g_value_init (gvalue, enforce_type);
	  g_value_set_object (gvalue, o);
	  return TRUE;
	}
      
      g_object_unref (o);
    }

  return FALSE;
}


GScriptValue *
g_script_value_get_property (GScriptValue *value,
			     const char *name)
{
  JSContext *cx = _g_script_get_internal_context ();
  GScriptValue *prop_value;

  prop_value = g_object_new (G_TYPE_SCRIPT_VALUE, NULL);
  if (g_script_value_is_object (value))
    {
      JS_GetProperty(cx, JSVAL_TO_OBJECT (value->value),
		     name, &prop_value->value);
    }
	  
  return prop_value;
}

void
g_script_value_set_property (GScriptValue *value,
			     const char *name,
			     GScriptValue *prop_val)
{
  JSContext *cx = _g_script_get_internal_context ();

  /* TODO: Check error? */
  if (g_script_value_is_object (value))
    {
      JS_SetProperty (cx, JSVAL_TO_OBJECT (value->value),
		      name, &prop_val->value);
    }
}
