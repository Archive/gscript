#ifndef __G_SCRIPT_VALUE_H__
#define __G_SCRIPT_VALUE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define G_TYPE_SCRIPT_VALUE         (g_script_value_get_type ())
#define G_SCRIPT_VALUE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), G_TYPE_SCRIPT_VALUE, GScriptValue))
#define G_SCRIPT_VALUE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), G_TYPE_SCRIPT_VALUE, GScriptValueClass))
#define G_IS_SCRIPT_VALUE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), G_TYPE_SCRIPT_VALUE))
#define G_IS_SCRIPT_VALUE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), G_TYPE_SCRIPT_VALUE))
#define G_SCRIPT_VALUE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), G_TYPE_SCRIPT_VALUE, GScriptValueClass))

typedef struct _GScriptValueClass   GScriptValueClass;
typedef struct _GScriptValue GScriptValue;

struct _GScriptValueClass
{
  GObjectClass parent_class;

  /* Padding for future expansion */
  void (*_g_reserved1) (void);
  void (*_g_reserved2) (void);
  void (*_g_reserved3) (void);
  void (*_g_reserved4) (void);
  void (*_g_reserved5) (void);
};

typedef GScriptValue * (*GScriptNativeFunction) (int n_args, GScriptValue **args);

GType g_script_value_get_type  (void) G_GNUC_CONST;

GScriptValue      *g_script_value_new (void);
GScriptValue      *g_script_value_new_undefined (void);
GScriptValue      *g_script_value_new_from_gvalue (GValue *value);
GScriptValue      *g_script_value_new_from_boolean (gboolean v);
GScriptValue      *g_script_value_new_from_double (double v);
GScriptValue      *g_script_value_new_from_int32 (gint32 v);
GScriptValue      *g_script_value_new_from_uint32 (guint32 v);
GScriptValue      *g_script_value_new_from_string (const char *str);
GScriptValue      *g_script_value_new_from_gobject (GObject *object);
GScriptValue      *g_script_value_new_from_function (GScriptNativeFunction function, int n_args);
GScriptValue      *g_script_value_dup (GScriptValue *value);

gboolean           g_script_value_is_null (GScriptValue *value);
gboolean           g_script_value_is_undefined (GScriptValue *value);
gboolean           g_script_value_is_boolean (GScriptValue *value);
gboolean           g_script_value_is_number (GScriptValue *value);
gboolean           g_script_value_is_string (GScriptValue *value);
gboolean           g_script_value_is_object (GScriptValue *value);
gboolean           g_script_value_is_function (GScriptValue *value);
gboolean           g_script_value_is_gobject (GScriptValue *value);

gboolean           g_script_value_to_boolean (GScriptValue *value);
gint32             g_script_value_to_int32 (GScriptValue *value);
guint32            g_script_value_to_uint32 (GScriptValue *value);
double             g_script_value_to_double (GScriptValue *value);
char *             g_script_value_to_string (GScriptValue *value);
GScriptValue *     g_script_value_to_object (GScriptValue *value);
GObject *          g_script_value_to_gobject (GScriptValue *value);

gboolean           g_script_value_to_gvalue (GScriptValue *value,
					     GType enforce_type,
					     GValue *gvalue);

GScriptValue *     g_script_value_get_property (GScriptValue *value,
						const char *name);
void               g_script_value_set_property (GScriptValue *value,
						const char *name,
						GScriptValue *prop_val);

/* remove property? */
G_END_DECLS

#endif /* __G_SCRIPT_VALUE_H__ */
