#include "gscriptinternal.h"

gboolean
_g_script_convert_type_supported (GITypeInfo *type_info)
{
  GITypeTag type_tag;

  type_tag = g_type_info_get_tag (type_info);

  switch (type_tag)
    {
    case GI_TYPE_TAG_VOID:
    case GI_TYPE_TAG_BOOLEAN:
    case GI_TYPE_TAG_INT8:
    case GI_TYPE_TAG_UINT8:
    case GI_TYPE_TAG_INT16:
    case GI_TYPE_TAG_UINT16:
    case GI_TYPE_TAG_INT32:
    case GI_TYPE_TAG_UINT32:
    case GI_TYPE_TAG_INT64:
    case GI_TYPE_TAG_UINT64:
    case GI_TYPE_TAG_INT:
    case GI_TYPE_TAG_UINT:
    case GI_TYPE_TAG_LONG:
    case GI_TYPE_TAG_ULONG:
    case GI_TYPE_TAG_SSIZE:
    case GI_TYPE_TAG_SIZE:
    case GI_TYPE_TAG_FLOAT:
    case GI_TYPE_TAG_DOUBLE:
    case GI_TYPE_TAG_UTF8:
      return TRUE;
      
    case GI_TYPE_TAG_FILENAME:
    case GI_TYPE_TAG_ARRAY:
    case GI_TYPE_TAG_GLIST:
    case GI_TYPE_TAG_GSLIST:
    case GI_TYPE_TAG_GHASH:
    case GI_TYPE_TAG_ERROR:
      return FALSE;
      
    case GI_TYPE_TAG_INTERFACE:
      {
	GIBaseInfo *interface;
	GIInfoType interface_type;
	
	interface = g_type_info_get_interface (type_info);
	interface_type = g_base_info_get_type (interface);

	if (interface_type == GI_INFO_TYPE_OBJECT)
	  return TRUE;
	
	return FALSE;
      }
    }
  
  return FALSE;
}

gboolean
_g_script_convert_js_to_arg (GScriptValue *value,
			     GITypeInfo *type_info,
			     GArgument *arg,
			     gboolean may_be_null,
			     char **error)
{
  GITypeTag type_tag;

  type_tag = g_type_info_get_tag (type_info);

  switch (type_tag)
    {
    case GI_TYPE_TAG_VOID:
      if (!g_script_value_is_undefined (value))
	{
	  *error = "Invalid argument type, expected undefined";
	  return FALSE;
	}
      break;
    case GI_TYPE_TAG_BOOLEAN:
      if (!g_script_value_is_boolean (value))
	{
	  *error = "Invalid argument type, expected boolean";
	  return FALSE;
	}
      arg->v_boolean = g_script_value_to_boolean (value);
      break;
    case GI_TYPE_TAG_INT8:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_int8 = g_script_value_to_int32 (value);
      break;
    case GI_TYPE_TAG_UINT8:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_uint8 = g_script_value_to_uint32 (value);
      break;
    case GI_TYPE_TAG_INT16:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_int16 = g_script_value_to_int32 (value);
      break;
    case GI_TYPE_TAG_UINT16:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_uint16 = g_script_value_to_uint32 (value);
      break;
    case GI_TYPE_TAG_INT32:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_int32 = g_script_value_to_int32 (value);
      break;
    case GI_TYPE_TAG_UINT32:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_uint32 = g_script_value_to_uint32 (value);
      break;
    case GI_TYPE_TAG_INT64:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_int64 = (gint64)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_UINT64:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_uint64 = (gint64)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_INT:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_int = g_script_value_to_int32 (value);
      break;
    case GI_TYPE_TAG_UINT:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_uint = g_script_value_to_uint32 (value);
      break;
    case GI_TYPE_TAG_LONG:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_long = (long)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_ULONG:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_ulong = (gulong)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_SSIZE:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_ssize = (gssize)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_SIZE:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_size = (gsize)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_FLOAT:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_float = (float)g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_DOUBLE:
      if (!g_script_value_is_number (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_double = g_script_value_to_double (value);
      break;
    case GI_TYPE_TAG_UTF8:
      if (may_be_null && g_script_value_is_null (value))
	{
	  arg->v_string = NULL;
	  break;
	}
      if (!g_script_value_is_string (value))
	{
	  *error = "Invalid argument type, expected number";
	  return FALSE;
	}
      arg->v_string = g_script_value_to_string (value);
      break;
    case GI_TYPE_TAG_FILENAME:
      *error = "filename arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_ARRAY:
      *error = "array arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_INTERFACE:
      {
	GIBaseInfo *interface;
	GIInfoType interface_type;
	GType required_gtype;
	GObject *gobject;
	
	interface = g_type_info_get_interface (type_info);
	interface_type = g_base_info_get_type (interface);

	arg->v_pointer = NULL;
	
	if (interface_type == GI_INFO_TYPE_OBJECT)
	  {
	    if (may_be_null && g_script_value_is_null (value))
	      {
		arg->v_pointer = NULL;
		break;
	      }
	    
	    if (!g_script_value_is_gobject (value))
	      {
		*error = "Invalid argument type, expected GObject";
		return FALSE;
	      }
	    
	    gobject = g_script_value_to_gobject (value);
	    required_gtype = g_registered_type_info_get_g_type ((GIRegisteredTypeInfo *)interface);
	    if (!g_type_is_a (G_OBJECT_TYPE (gobject), required_gtype))
	      {
		*error = "Invalid argument type, not right GType";
		return FALSE;
	      }
	    arg->v_pointer = g_object_ref (gobject);
	    break;
	  }
	else
	  {
	    g_print ("Unhandled interface type: %d\n", interface_type);
	  }
	
	*error = "interface arguments of not yet supported type";
	return FALSE;
      }
      *error = "interface arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_GLIST:
      *error = "glist arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_GSLIST:
      *error = "gslist arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_GHASH:
      *error = "ghash arguments not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_ERROR:
      *error = "error arguments not yet supported";
      return FALSE;
      break;
    }
  
  return TRUE;
}

gboolean
_g_script_convert_arg_to_js (GArgument *arg,
			     GITypeInfo *type_info,
			     GScriptValue **value,
			     char **error)
{
  GITypeTag type_tag;

  type_tag = g_type_info_get_tag (type_info);

  switch (type_tag)
    {
    case GI_TYPE_TAG_VOID:
      *value = g_script_value_new_undefined ();
      break;
    case GI_TYPE_TAG_BOOLEAN:
      *value = g_script_value_new_from_boolean (arg->v_boolean);
      break;
    case GI_TYPE_TAG_INT8:
      *value = g_script_value_new_from_int32 (arg->v_int8);
      break;
    case GI_TYPE_TAG_UINT8:
      *value = g_script_value_new_from_uint32 (arg->v_uint8);
      break;
    case GI_TYPE_TAG_INT16:
      *value = g_script_value_new_from_int32 (arg->v_int16);
      break;
    case GI_TYPE_TAG_UINT16:
      *value = g_script_value_new_from_uint32 (arg->v_uint16);
      break;
    case GI_TYPE_TAG_INT32:
      *value = g_script_value_new_from_int32 (arg->v_int32);
      break;
    case GI_TYPE_TAG_UINT32:
      *value = g_script_value_new_from_uint32 (arg->v_uint32);
      break;
    case GI_TYPE_TAG_INT64:
      *value = g_script_value_new_from_double (arg->v_int64);
      break;
    case GI_TYPE_TAG_UINT64:
      *value = g_script_value_new_from_double (arg->v_uint64);
      break;
    case GI_TYPE_TAG_INT:
      *value = g_script_value_new_from_int32 (arg->v_int32);
      break;
    case GI_TYPE_TAG_UINT:
      *value = g_script_value_new_from_uint32 (arg->v_uint32);
      break;
    case GI_TYPE_TAG_LONG:
      *value = g_script_value_new_from_double (arg->v_long);
      break;
    case GI_TYPE_TAG_ULONG:
      *value = g_script_value_new_from_double (arg->v_ulong);
      break;
    case GI_TYPE_TAG_SSIZE:
      *value = g_script_value_new_from_double (arg->v_ssize);
      break;
    case GI_TYPE_TAG_SIZE:
      *value = g_script_value_new_from_double (arg->v_size);
      break;
    case GI_TYPE_TAG_FLOAT:
      *value = g_script_value_new_from_double (arg->v_float);
      break;
    case GI_TYPE_TAG_DOUBLE:
      *value = g_script_value_new_from_double (arg->v_double);
      break;
    case GI_TYPE_TAG_UTF8:
      *value = g_script_value_new_from_string (arg->v_string);
      break;
    case GI_TYPE_TAG_FILENAME:
      *error = "filename return values not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_ARRAY:
      *error = "array return values not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_INTERFACE:
      {
	GIBaseInfo *interface;
	GIInfoType interface_type;
	
	interface = g_type_info_get_interface (type_info);
	interface_type = g_base_info_get_type (interface);
	
	if (interface_type == GI_INFO_TYPE_OBJECT)
	  {
	    *value = g_script_value_new_from_gobject (arg->v_pointer);
	    break;
	  }
	else
	  {
	    g_print ("Unhandled interface type: %d\n", interface_type);
	  }
	
	*error = "interface return values of not yet supported type";
	return FALSE;
      }
      break;
    case GI_TYPE_TAG_GLIST:
      *error = "glist return values not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_GSLIST:
      *error = "gslist return values not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_GHASH:
      *error = "ghash return values not yet supported";
      return FALSE;
      break;
    case GI_TYPE_TAG_ERROR:
      *error = "error return values not yet supported";
      return FALSE;
      break;
    }

  return TRUE;
}

void
_g_script_arg_free (GArgument *arg,
		    GITypeInfo *type_info,
		    gboolean free_deep)
{
  GITypeTag type_tag;

  type_tag = g_type_info_get_tag (type_info);

  switch (type_tag)
    {
    case GI_TYPE_TAG_UTF8:
      g_free (arg->v_string);
      break;
    case GI_TYPE_TAG_FILENAME:
      g_free (arg->v_string);
      break;
    case GI_TYPE_TAG_ARRAY:
      g_warning ("TODO");
      break;
    case GI_TYPE_TAG_INTERFACE:
      {
	GIBaseInfo *interface;
	GIInfoType interface_type;
	
	interface = g_type_info_get_interface (type_info);
	interface_type = g_base_info_get_type (interface);
	
	if (interface_type == GI_INFO_TYPE_OBJECT)
	  {
	    if (arg->v_pointer)
	      g_object_unref (arg->v_pointer);
	    break;
	  }
	else
	  g_warning ("TODO");
      }
      break;
    case GI_TYPE_TAG_GLIST:
      g_warning ("TODO");
      break;
    case GI_TYPE_TAG_GSLIST:
      g_warning ("TODO");
      break;
    case GI_TYPE_TAG_GHASH:
      g_warning ("TODO");
      break;
    case GI_TYPE_TAG_ERROR:
      g_warning ("TODO");
      break;
    default:
      break;
    }
}
