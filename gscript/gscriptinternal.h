#ifndef __G_SCRIPT_INTERNAL_H__
#define __G_SCRIPT_INTERNAL_H__

#include <jsapi.h>
#include "gscriptvalue.h"
#include "gscriptengine.h"
#include <girepository.h>

JSRuntime *_g_script_get_runtime (void);
JSContext *_g_script_get_internal_context (void);
JSObject *_g_script_get_gtype_object (void);
jsval _g_script_value_get_jsval (GScriptValue *value);
GScriptValue * _g_script_value_new_from_jsval (jsval value);
GScriptValue * _g_script_value_new_from_jsobject (JSObject *obj);
GObject * _js_object_get_gobject (JSObject *obj);

gboolean wrap_object (GObject *object,
		      JSObject **res /* Should be rooted */
		      );

/* Introspection arg conversion: */
gboolean _g_script_convert_type_supported (GITypeInfo *type_info);
gboolean _g_script_convert_js_to_arg (GScriptValue *value,
				      GITypeInfo *type_info,
				      GArgument *arg,
				      gboolean may_be_null,
				      char **error);
gboolean _g_script_convert_arg_to_js (GArgument *arg,
				      GITypeInfo *type_info,
				      GScriptValue **value,
				      char **error);
void     _g_script_arg_free          (GArgument *arg,
				      GITypeInfo *type_info,
				      gboolean free_deep);


#endif /* __G_SCRIPT_INTERNAL_H__ */
