#include "gscriptengine.h"
#include "gscriptinternal.h"
#include <string.h>

/**
 * GScriptEngine:
 * 
 */
struct _GScriptEngine
{
  GObject parent_instance;

  JSContext *cx;
  JSObject *global;
};

G_DEFINE_TYPE (GScriptEngine, g_script_engine, G_TYPE_OBJECT);

static void
g_script_engine_finalize (GObject *object)
{
  GScriptEngine *engine;

  engine = G_SCRIPT_ENGINE (object);

  JS_RemoveRoot (engine->cx, &engine->global);
  
  JS_DestroyContext (engine->cx);
  engine->cx = NULL;

  G_OBJECT_CLASS (g_script_engine_parent_class)->finalize (object);
}

static void
g_script_engine_class_init (GScriptEngineClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = g_script_engine_finalize;
}

static void
error_reporter (JSContext *cx, const char *message, JSErrorReport *report)
{
  /* TODO: Better support for error reporting. Emit signal? */
  fprintf(stderr, "JS error: %s (at line %d)\n", message, report->lineno);
}

static JSBool
GC (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
    JS_GC(cx);
    return JS_TRUE;
}

static void
g_script_engine_init (GScriptEngine *engine)
{
  jsval gtype;
  static JSClass global_class = {
    "global", JSCLASS_GLOBAL_FLAGS,
    JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
    JS_EnumerateStub,JS_ResolveStub,JS_ConvertStub,JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
  };

  static JSFunctionSpec global_functions[] = {
    {"__gc",              GC,             0,0,0},
    {NULL,NULL,0,0,0}
  };
  
  
  engine->cx = JS_NewContext (_g_script_get_runtime (), 8192);
  JS_SetContextPrivate (engine->cx, engine);
  
  JS_SetOptions(engine->cx, JSOPTION_VAROBJFIX);
  JS_SetVersion(engine->cx, JSVERSION_1_7);
  
  engine->global = NULL;
  JS_AddNamedRoot (engine->cx, &engine->global, "GScriptEngine->global");

  JS_SetErrorReporter(engine->cx, error_reporter);

  engine->global = JS_NewObject (engine->cx, &global_class, NULL, NULL);
  
  JS_InitStandardClasses (engine->cx, engine->global);

  gtype = OBJECT_TO_JSVAL (_g_script_get_gtype_object ());
  if (!JS_SetProperty (engine->cx, engine->global, "GType", &gtype))
    g_warning ("Failed to create GType global property");

  /* TODO: Move these out of GScriptEngine */
  JS_DefineFunctions (engine->cx, engine->global, global_functions);
}

/**
 * g_script_engine_new:
 * 
 * Creates a new script engine
 *
 * Returns: a #GScriptEngine.
 **/
GScriptEngine *
g_script_engine_new (void)
{
  return g_object_new (G_TYPE_SCRIPT_ENGINE, NULL);
}

GScriptValue *
g_script_engine_get_global (GScriptEngine *engine)
{
  return _g_script_value_new_from_jsobject (engine->global);
}

GScriptValue *
g_script_engine_evaluate_script (GScriptEngine *engine,
				 const char *script)
{
  jsval rval;

  /* TODO:
     error reporting,
     utf8-> utf16
     filenames..
     roots
  */

  rval = JSVAL_VOID;
  JS_EvaluateScript (engine->cx, engine->global, script, strlen(script),
		     "filename", 0, &rval);

  return _g_script_value_new_from_jsval (rval);
}

GScriptValue *
g_script_engine_call (GScriptEngine  *engine,
		      GScriptValue   *this_object,
		      GScriptValue   *function,
		      guint           n_args,
		      GScriptValue **args)
{
  jsval f, ret, *js_args;
  GScriptValue *ret_val;
  int i;
  
  if (!g_script_value_is_function (function))
    return g_script_value_new_undefined ();

  if (this_object != NULL && !g_script_value_is_object (function))
    return g_script_value_new_undefined ();
  
  f = _g_script_value_get_jsval (function);

  JS_AddRoot (engine->cx, &ret);
  js_args = g_new (jsval, n_args);
  for (i = 0; i < n_args; i++)
    js_args[i] = _g_script_value_get_jsval (args[i]);

  ret = JSVAL_VOID;
  if (!JS_CallFunctionValue (engine->cx,
			     (this_object != NULL) ?
			       JSVAL_TO_OBJECT (_g_script_value_get_jsval (this_object)) :
			       engine->global,
			     f, n_args, 
			     js_args, &ret))
    g_warning ("Call failed");

  g_free (js_args);
  ret_val = _g_script_value_new_from_jsval (ret);
  
  return ret_val;
}
