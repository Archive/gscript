#ifndef __G_SCRIPT_ENGINE_H__
#define __G_SCRIPT_ENGINE_H__

#include <glib-object.h>
#include <gscript/gscriptvalue.h>

G_BEGIN_DECLS

#define G_TYPE_SCRIPT_ENGINE         (g_script_engine_get_type ())
#define G_SCRIPT_ENGINE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), G_TYPE_SCRIPT_ENGINE, GScriptEngine))
#define G_SCRIPT_ENGINE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), G_TYPE_SCRIPT_ENGINE, GScriptEngineClass))
#define G_IS_SCRIPT_ENGINE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), G_TYPE_SCRIPT_ENGINE))
#define G_IS_SCRIPT_ENGINE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), G_TYPE_SCRIPT_ENGINE))
#define G_SCRIPT_ENGINE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), G_TYPE_SCRIPT_ENGINE, GScriptEngineClass))

typedef struct _GScriptEngineClass   GScriptEngineClass;
typedef struct _GScriptEngine GScriptEngine;

struct _GScriptEngineClass
{
  GObjectClass parent_class;

  /* Padding for future expansion */
  void (*_g_reserved1) (void);
  void (*_g_reserved2) (void);
  void (*_g_reserved3) (void);
  void (*_g_reserved4) (void);
  void (*_g_reserved5) (void);
};

GType g_script_engine_get_type  (void) G_GNUC_CONST;

GScriptEngine      *g_script_engine_new (void);
GScriptValue *g_script_engine_get_global (GScriptEngine *engine);
GScriptValue *g_script_engine_evaluate_script (GScriptEngine *engine,
					       const char *script);
GScriptValue *     g_script_engine_call (GScriptEngine  *engine,
					 GScriptValue   *this_object,
					 GScriptValue   *function,
					 guint           n_args,
					 GScriptValue   **args);

G_END_DECLS

#endif /* __G_SCRIPT_ENGINE_H__ */
