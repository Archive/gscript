#include "gscriptinternal.h"
#include <girepository.h>
#include <string.h>

static void finalize_signal_object (JSContext *cx,
				    JSObject *obj);
static JSBool lazy_enumerate_object (JSContext *cx, JSObject *obj);
static JSBool lazy_resolve_object (JSContext *cx, JSObject *obj, jsval id,  uintN flags, JSObject **objp);
static void jsclass_finalize_gobject (JSContext *cx,
				      JSObject *obj);
static JSBool lazy_enumerate_object_prototype (JSContext *cx, JSObject *obj);
static JSBool lazy_resolve_object_prototype (JSContext *cx, JSObject *obj, jsval id,  uintN flags, JSObject **objp);
static void jsclass_finalize_gobject_prototype (JSContext *cx,
						JSObject *obj);
static JSObject *ensure_class_for_gtype (GType type);
static JSBool call_signal_object (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval);

typedef struct {
  guint signal_id;
} SignalPrivate;

typedef struct {
  GClosure	closure;
  GScriptEngine *engine;
  GScriptValue *function;
} JSClosure;

static JSClass signal_class = {
  "signal", JSCLASS_HAS_PRIVATE ,
  JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
  JS_EnumerateStub,JS_ResolveStub, JS_ConvertStub, finalize_signal_object,
  0,0,
  call_signal_object
};

static JSClass gobject_class = {
  "gobject",
  JSCLASS_HAS_PRIVATE | JSCLASS_NEW_RESOLVE | JSCLASS_NEW_RESOLVE_GETS_START,
  JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
  lazy_enumerate_object,(JSResolveOp)lazy_resolve_object,
  JS_ConvertStub, jsclass_finalize_gobject,
};

static JSClass gobject_prototype_class = {
  "gobject_prototype",
  JSCLASS_HAS_PRIVATE | JSCLASS_NEW_RESOLVE | JSCLASS_NEW_RESOLVE_GETS_START,
  JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
  lazy_enumerate_object_prototype,(JSResolveOp)lazy_resolve_object_prototype,
  JS_ConvertStub, jsclass_finalize_gobject_prototype,
};

static JSBool
throw_error (JSContext *cx, const char *format, ...)
{
  void *mark;
  jsval *args;
  jsval exc;
  gchar *buffer;
  va_list vargs;

  va_start (vargs, format);
  buffer = g_strdup_vprintf (format, vargs);
  va_end (vargs);
  
  args = JS_PushArguments (cx, &mark, "s", buffer);
  g_free (buffer);
  
  if (args)
    {
      if (JS_CallFunctionName (cx, JS_GetGlobalObject (cx),
			      "Error", 1, args, &exc))
	JS_SetPendingException (cx, exc);
      JS_PopArguments (cx, mark);
    }
  
  return JS_FALSE;
}

GObject *
_js_object_get_gobject (JSObject *obj)
{
  JSContext *cx;
  
  cx = _g_script_get_internal_context ();
  return JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
}

static char *
canonical_property_name_to_js_name (const char *canonical, const char *prefix)
{
  char *js_name;
  char *p;
  int len, prefix_len;
  
  len = strlen (canonical);
  prefix_len = 0;
  if (prefix)
    prefix_len = strlen (prefix);
  js_name = g_malloc (len + prefix_len + 1);
  if (prefix)
    strcpy (js_name, prefix);
  strcpy (js_name + prefix_len, canonical);
  
  for (p = js_name; *p != 0; p++)
    {
      if (*p == '-')
	*p = '_';
    }

  return js_name;
}

JSRuntime *
_g_script_get_runtime (void)
{
  static JSRuntime *rt = NULL;

  if (rt == NULL)
    rt = JS_NewRuntime (0x100000);

  return rt;
}

static void
list_children_types (GArray *array, GType parent)
{
  GType *children;
  guint n_children, i;
  
  children = g_type_children (parent, &n_children);
  g_array_append_vals (array, children, n_children);

  for (i = 0; i < n_children; i++)
    list_children_types (array, children[i]);
  
  g_free (children);
}

typedef struct {
  GType *types;
  int n_types;
  int i;
} GTypeList;

static JSBool
lazy_enumerate_gtype (JSContext *cx, JSObject *obj,
		      JSIterateOp enum_op, jsval *statep, jsid *idp)
{
  GArray *array;
  GTypeList *list;
  GType t;

  switch (enum_op)
    {
    case JSENUMERATE_INIT:
      array = g_array_new (FALSE, FALSE, sizeof (GType));
      t = G_TYPE_OBJECT;
      g_array_append_val (array, t);
      list_children_types (array, G_TYPE_OBJECT);

      list = g_new (GTypeList, 1);
      list->n_types = array->len;
      list->types = (GType *)g_array_free (array, FALSE);
      list->i = 0;

      *statep = PRIVATE_TO_JSVAL (list);
      if (idp)
	*idp = INT_TO_JSVAL(list->n_types);
      break;
    case JSENUMERATE_NEXT:
      list = JSVAL_TO_PRIVATE (*statep);
      if (list->i < list->n_types)
	{
	  JSString *s;
	  s = JS_NewStringCopyZ (cx, g_type_name (list->types[list->i++]));
	  JS_ValueToId (cx, STRING_TO_JSVAL(s), idp);
	}
      else
	*statep = JSVAL_NULL; 
      break;
    case JSENUMERATE_DESTROY:
      list = JSVAL_TO_PRIVATE (*statep);
      g_free (list->types);
      g_free (list);
      *statep = JSVAL_NULL; 
      break;
    }
  return JS_TRUE;
}

static JSBool
lazy_resolve_gtype (JSContext *cx, JSObject *obj, jsval id)
{
  const char *propname;
  GType type;
  
  g_assert (JSVAL_IS_STRING (id));
  
  propname = JS_GetStringBytes (JSVAL_TO_STRING (id));

  type = G_TYPE_INVALID;
  if (propname)
    type = g_type_from_name (propname);

  if (type)
    ensure_class_for_gtype (type);
  
  return JS_TRUE;
}

static JSBool
gtype_import_namespace (JSContext *cx,
			JSObject *obj,
			uintN argc,
			jsval *argv,
			jsval *rval)
{
  const char *ns;
  int i, n;
  GIBaseInfo *info;
  
  if (!JSVAL_IS_STRING (argv[0]))
    return JS_FALSE; /* TODO: Set error */
  
  ns = JS_GetStringBytes (JSVAL_TO_STRING (argv[0]));

  g_irepository_require (g_irepository_get_default (), ns, 0, NULL);

  n = g_irepository_get_n_infos (g_irepository_get_default (), ns);
  for (i = 0; i < n; i++)
    {
      info = g_irepository_get_info (g_irepository_get_default (), ns, i);
      if (g_base_info_get_type (info) == GI_INFO_TYPE_OBJECT)
	{
	  GType type;

	  type = g_registered_type_info_get_g_type ((GIRegisteredTypeInfo *)info);

	  if (type != 0)
	    ensure_class_for_gtype (type);
	}
    }
  return JS_TRUE;
}

JSObject *
_g_script_get_gtype_object (void)
{
  static JSObject *gtype = NULL;
  static JSClass gtype_class = {
    "gtype", JSCLASS_NEW_ENUMERATE,
    JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
    (JSEnumerateOp)lazy_enumerate_gtype, lazy_resolve_gtype,JS_ConvertStub,JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
  };
  static JSFunctionSpec gtype_methods[] = {
    {"import_namespace",  gtype_import_namespace, 1,JSPROP_ENUMERATE,0},
    {NULL,NULL,0,0,0}
  };
  JSContext *cx;

  if (gtype == NULL)
    {
      cx = _g_script_get_internal_context ();
      
      JS_AddRoot (cx, &gtype);
      
      gtype = JS_NewObject (cx, &gtype_class, NULL, NULL);

      JS_DefineFunctions (cx, gtype, gtype_methods);      

    }

  return gtype;
}

JSContext *
_g_script_get_internal_context (void)
{
  JSRuntime *rt;
  static JSContext *cx = NULL;
  JSObject *global;
  jsval gtype;
  static JSClass global_class = {
    "global", JSCLASS_GLOBAL_FLAGS,
    JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,JS_PropertyStub,
    JS_EnumerateStub,JS_ResolveStub,JS_ConvertStub,JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
  };

  if (cx == NULL)
    {
      rt = _g_script_get_runtime ();
      cx = JS_NewContext (rt, 0x1000);
      global = JS_NewObject (cx, &global_class, NULL, NULL);
      JS_InitStandardClasses (cx, global);

      gtype = OBJECT_TO_JSVAL (_g_script_get_gtype_object ());
      if (!JS_SetProperty (cx, global, "GType", &gtype))
	g_error ("Unable to init GType object");
    }

  return cx;
}

static void object_toggle_notify_cb (gpointer      data,
				     GObject      *object,
				     gboolean      is_last_ref);


static JSBool
construct_object (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
  gpointer private;
  GType type;
  GObject *gobject;
  JSObject *o;
  int i, n_params;
  GParameter *params;
  gboolean res;
  GParamSpec *param_spec;
  GObjectClass *oclass;
  const char *propname;
  GScriptValue *v;
    
  private =  JS_GetInstancePrivate (cx, JS_GetPrototype(cx, obj), &gobject_prototype_class, NULL);

  if (private == NULL)
    return throw_error (cx, "No GType for this constructor");

  type = (GType)private;
  oclass = g_type_class_peek (type);
  
  /* Handle construct properties */
  
  params = g_new0 (GParameter, (argc+1) / 2);

  res = TRUE;
  i = 0;
  n_params = 0;
  while (i < argc)
    {
      if (!JSVAL_IS_STRING (argv[i]))
	{
	  res = throw_error (cx, "expected string for property name");
	  goto out;
	}
      propname = JS_GetStringBytes (JSVAL_TO_STRING (argv[i]));
      i++;
      if (i == argc)
	{
	  res = throw_error (cx, "no value for property %s", propname);
	  goto out;
	}

      param_spec = g_object_class_find_property (oclass, propname);
      if (param_spec != NULL)
	{
	  v = _g_script_value_new_from_jsval (argv[i]);
	  
	  if (!g_script_value_to_gvalue (v,
					 G_PARAM_SPEC_VALUE_TYPE (param_spec),
					 &params[n_params].value))
	    {
	      g_object_unref (v);
	      res = throw_error (cx, "Argument of wrong type for the property %s", propname);
	      goto out;
	    }
	  params[n_params++].name = propname;
	  g_object_unref (v);
	}
      else
	{
	  res = throw_error (cx, "no such property %s", propname);
	  goto out;
	}

      i++;
    }
  
  
  gobject = g_object_newv (type, n_params, params);
  if (gobject == NULL)
    {
      res = throw_error (cx, "GObject construction failed");
      goto out;
    }

  g_object_ref_sink (gobject);

  JS_AddRoot (cx, &o);
  if (wrap_object (gobject, &o))
    *rval = OBJECT_TO_JSVAL (o);
  
  JS_RemoveRoot (cx, &o);
  
  g_object_unref (gobject);

 out:
  
  for (i = 0; i < n_params; i++)
    g_value_unset (&params[i].value);

  g_free (params);
  
  return res;
}

static void
jsclass_finalize_gobject_prototype (JSContext *cx,
				    JSObject *obj)
{
}


static void
jsclass_finalize_gobject (JSContext *cx,
			  JSObject *obj)
{
  GObject *wrapped_object;

  wrapped_object = JS_GetPrivate (cx, obj);
  
  if (wrapped_object == NULL)
    return; /* Really shouldn't happen, but lets be safe */

  //g_print ("Finalizing proxy %p (wrapped object %p)\n", obj, wrapped_object);
  
  /* We're finalizing the js proxy object, and we're about to
   * drop the last ref to the wrapped object, freeing it.
   * However, its possible that the unref ressurects the
   * wrapped object, and for that eventuallity we remove the
   * js proxy pointer first so that it won't be around pointing
   * to an invalid object.
   */
  g_object_set_data_full (wrapped_object,
			  "js-wrapper",
			  NULL, NULL);
  
  /* Release the wrapped object, freeing it */
  g_object_remove_toggle_ref (wrapped_object,
			      object_toggle_notify_cb,
			      cx);
  
  /* At this point the private pointer of the proxy still points to
   * the dead wrapped object, but the proxy is finalized and won't be
   * referenced anymore, so it won't be used.
   */
}

static JSBool
object_get_property (JSContext *cx, JSObject *obj, jsval id, jsval *vp)
{
  GObject *wrapped_object;
  char *propname;
  GValue val = {0};
  GParamSpec *param_spec;
  GScriptValue *v;

  g_assert (JSVAL_IS_STRING (id));

  propname = JS_GetStringBytes (JSVAL_TO_STRING (id));

  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return JS_TRUE; /* This may be a prototype, we can't get the property value, set to undefined */
  
  param_spec = g_object_class_find_property (G_OBJECT_GET_CLASS (wrapped_object),
					     propname);
  g_value_init (&val, G_PARAM_SPEC_VALUE_TYPE (param_spec));
  g_object_get_property (wrapped_object,
			 propname,
			 &val);
  
  v = g_script_value_new_from_gvalue (&val);

  if (v == NULL)
    g_warning ("Failed to convert GValue of type %s in property %s", G_VALUE_TYPE_NAME (&val), propname);
  else
    {
      *vp = _g_script_value_get_jsval (v);
      g_object_unref (v);
    }
  
  g_value_unset (&val);  
  
  return JS_TRUE;
}

static JSBool
object_set_property (JSContext *cx, JSObject *obj, jsval id, jsval *vp)
{
  GObject *wrapped_object;
  char *propname;
  GValue val = {0};
  GParamSpec *param_spec;
  GScriptValue *v;

  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return JS_TRUE; /* Ignore proxies and wrong types */
  
  if (!JSVAL_IS_STRING (id))
    return JS_TRUE; /* Not a string, so not a gobject property */
  
  propname = JS_GetStringBytes (JSVAL_TO_STRING (id));

  param_spec = g_object_class_find_property (G_OBJECT_GET_CLASS (wrapped_object),
					     propname);
  if (param_spec != NULL)
    {
      v = _g_script_value_new_from_jsval (*vp);
      
      /* TODO: Convert v to type of param_spec and put in val */
      if (!g_script_value_to_gvalue (v,
				     G_PARAM_SPEC_VALUE_TYPE (param_spec),
				     &val))
	{
	  g_object_unref (v);
	  return throw_error (cx, "Argument of wrong type for the property %s", propname); 
	}
      g_object_set_property (wrapped_object,
			     propname,
			     &val);
      g_object_unref (v);
      g_value_unset (&val);
    }
  
  return JS_TRUE;
}

static gboolean
define_property_from_paramspec (JSContext *cx, JSObject *obj, GParamSpec *param_spec)
{
  static GQuark qname = 0;
  guint attrs;
  char *js_name;

  if (qname == 0)
    qname = g_quark_from_static_string ("js-defined-property");

  if (g_param_spec_get_qdata (param_spec, qname) != NULL)
    return FALSE; /* Already defined, ignore request */
  
  g_param_spec_set_qdata (param_spec, qname, (gpointer)1);
  
  if (param_spec->flags & G_PARAM_CONSTRUCT_ONLY)
    return FALSE;
  
  attrs = JSPROP_ENUMERATE | JSPROP_PERMANENT | JSPROP_SHARED;
  
  if ((param_spec->flags & G_PARAM_WRITABLE) == 0)
    attrs |= JSPROP_READONLY;

  js_name = canonical_property_name_to_js_name (param_spec->name, NULL);
  
  /* If this fails we just don't find this property, not
     much we can do. Better luck next time. */
  JS_DefineProperty (cx, obj,
		     js_name,
		     JSVAL_VOID,
		     object_get_property,
		     object_set_property,
		     attrs);

  g_free (js_name);
  
  return TRUE;
}

static void
finalize_signal_object (JSContext *cx,
			JSObject *obj)
{
  SignalPrivate *private;
  
  private = JS_GetPrivate (cx, obj);
  g_free (private);
}

static void
free_js_closure (gpointer	 data,
		 GClosure	*closure)
{
  JSClosure *js_closure = (JSClosure *)closure;

  g_object_unref (js_closure->engine);
  g_object_unref (js_closure->function);
}

static void
signal_closure_marshal_func (GClosure	*closure,
			     GValue     *return_value,
			     guint       n_param_values,
			     const GValue   *param_values,
			     gpointer    invocation_hint,
			     gpointer	 marshal_data)
{
  JSClosure *js_closure = (JSClosure *)closure;
  GScriptValue *ret;
  GScriptValue **args;
  int i;
  
  args = g_new0 (GScriptValue *, n_param_values);

  for (i = 0; i < n_param_values; i++)
    {
      args[i] = g_script_value_new_from_gvalue ((GValue *)&param_values[i]);
      
      if (args[i] == NULL)
	{
	  g_print ("Failed to convert arg %d in signal emission\n", i);
	  args[i] = g_script_value_new_undefined ();
	}
    }
  
  ret = g_script_engine_call (js_closure->engine,
			      args[0],
			      js_closure->function,
			      n_param_values - 1,
			      args+1);

  for (i = 0; i < n_param_values; i++)
    g_object_unref (args[i]);
  
  g_free (args);
}

static JSBool
call_signal_object (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
  JSObject *proxy_object;
  GObject *wrapped_object;
  SignalPrivate *private;
  GScriptValue *v;
  GValue *args;
  GValue retv = {0};
  int i;
  GSignalQuery query;
  GType type;
  gboolean types_ok;
  JSObject *signal_object;

  /* This is the object that contains the signal, the actual signal
     object is found at argv[-2] as the Function object */
  signal_object = JSVAL_TO_OBJECT (argv[-2]);

  private = JS_GetInstancePrivate (cx, signal_object, &signal_class, argv);
  if (private == NULL)
    return JS_FALSE;

  proxy_object = JS_GetParent (cx, signal_object); /* TODO: Verify ok (maybe we can set it non-modifiable) */
  
  wrapped_object = JS_GetInstancePrivate (cx, proxy_object, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return JS_FALSE;

  g_signal_query (private->signal_id, &query);

  if (argc < query.n_params)
    return throw_error (cx, "Too few arguments to signal %s", query.signal_name); 
  
  args = g_new0 (GValue, query.n_params + 1);
  
  g_value_init (&args[0], G_OBJECT_TYPE (wrapped_object));
  g_value_set_object (&args[0], wrapped_object);

  types_ok = TRUE;
  for (i = 0; types_ok && i < query.n_params; i++)
    {
      v = _g_script_value_new_from_jsval (argv[i]);
      
      type = query.param_types[i] & ~G_SIGNAL_TYPE_STATIC_SCOPE;
      
      if (!g_script_value_to_gvalue (v, type, &args[i+1]))
	types_ok = FALSE;

      g_object_unref (v);
    }

  if (types_ok)
    {
      type = query.return_type & ~G_SIGNAL_TYPE_STATIC_SCOPE;
      if (type != G_TYPE_NONE)
	g_value_init (&retv, type);
      
      g_signal_emitv (args, private->signal_id, 0, &retv);

      if (type != G_TYPE_NONE)
	{
	  v = g_script_value_new_from_gvalue (&retv);
	  if (v)
	    {
	      *rval = _g_script_value_get_jsval (v);
	      g_object_unref (v);
	    }
	  else
	    *rval = JSVAL_VOID;
	  
	  g_value_unset (&retv);
	}
      else
	*rval = JSVAL_VOID;
    }
  else
    *rval = JSVAL_VOID;

  for (i = 0; i < query.n_params+1; i++)
    g_value_unset (&args[i]);

  g_free (args);
      
  return JS_TRUE;
}


static JSBool
signal_object_connect (JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
  JSObject *proxy_object;
  GObject *wrapped_object;
  SignalPrivate *private;
  GClosure *closure;
  gulong res;

  private = JS_GetInstancePrivate (cx, obj, &signal_class, argv);
  if (private == NULL)
    return JS_FALSE;
  
  if (argv[0] == JSVAL_NULL ||
      !JSVAL_IS_OBJECT (argv[0]) ||
      !JS_ObjectIsFunction(cx, JSVAL_TO_OBJECT (argv[0])))
    return JS_FALSE; /* TODO: Set error */
  
  proxy_object = JS_GetParent (cx, obj); /* TODO: Verify ok (maybe we can set it non-modifiable) */
  
  wrapped_object = JS_GetInstancePrivate (cx, proxy_object, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return JS_FALSE;

  closure = g_closure_new_simple (sizeof (JSClosure), NULL);
  g_closure_set_marshal (closure, signal_closure_marshal_func);
  g_closure_add_finalize_notifier (closure, NULL, free_js_closure);
  ((JSClosure *)closure)->engine = JS_GetContextPrivate (cx);
  ((JSClosure *)closure)->function = _g_script_value_new_from_jsval (argv[0]);
  
  res = g_signal_connect_closure_by_id (wrapped_object,
					private->signal_id,
					0 /* detail */,
					closure,
					FALSE /* after */);

  return JS_TRUE;
}

static JSBool
signal_object_to_string (JSContext *cx,
			 JSObject *obj,
			 uintN argc,
			 jsval *argv,
			 jsval *rval)
{
  SignalPrivate *private;
  GObject *wrapped_object;
  char *str;
  GScriptValue *v;
  GSignalQuery query;
  JSObject *proxy_object;

  str = NULL;
  
  private =  JS_GetInstancePrivate (cx, obj, &signal_class, NULL);
  if (private == NULL)
    return JS_FALSE;

  proxy_object = JS_GetParent (cx, obj); /* TODO: Verify ok (maybe we can set it non-modifiable) */
  wrapped_object = JS_GetInstancePrivate (cx, proxy_object, &gobject_class, NULL);

  g_signal_query (private->signal_id, &query);
  str = g_strdup_printf ("[signal %s on %s@%p]",
			 query.signal_name,
			 g_type_name (G_OBJECT_TYPE (wrapped_object)),
			 wrapped_object);

  v = g_script_value_new_from_string (str);
  *rval = _g_script_value_get_jsval (v);
  g_object_unref (v);
  g_free (str);
  
  return TRUE;
}


static void
define_property_for_signal (JSContext *cx, JSObject *obj, const char *name, guint signal_id)
{
  guint attrs;
  JSObject *signal_object, *proto;
  SignalPrivate *private;
  char *js_name;
      
  attrs = JSPROP_ENUMERATE | JSPROP_PERMANENT | JSPROP_READONLY;

  /* If this fails we just don't find this property, not
     much we can do. Better luck next time. */

  js_name = canonical_property_name_to_js_name (name, "on_");

  signal_object = JS_DefineObject (cx, obj, js_name, &signal_class, NULL, attrs);
  
  g_free (js_name);

  if (signal_object != NULL)
    {
      private = g_new (SignalPrivate, 1);
      private->signal_id = signal_id;
      JS_SetPrivate (cx, signal_object, private);
      
      proto = JS_GetPrototype (cx, signal_object);

      JS_DefineFunction (cx, proto, 
			 "connect", signal_object_connect, 1, JSPROP_ENUMERATE);
      JS_DefineFunction (cx, proto, 
			 "toString", signal_object_to_string, 1, JSPROP_ENUMERATE);
    }
}

static JSBool
invoke_method (JSContext *cx,
	       JSObject *obj,
	       uintN argc,
	       jsval *argv,
	       jsval *rval)
{
  GObject *wrapped_object;
  GIFunctionInfo **infop, *info;
  jsval v;
  GError *error;
  GArgument retval;
  GArgument *in_args;
  GArgument *out_args;
  int n_args, n_in_args, n_out_args, i;
  GIArgInfo *arg_info;
  GITypeInfo *type_info;
  GIDirection dir;
  gboolean result;
  GScriptValue *script_value;
  char *err_str;

  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return throw_error (cx, "Invoking GObject method on non-GObject");

  if (!JS_GetReservedSlot (cx, JS_GetFunctionObject (JS_ValueToFunction (cx, argv[-2])), 0, &v))
    return throw_error (cx, "No method info availible for function call");

  infop = JSVAL_TO_PRIVATE (v);
  info = *infop;

  n_args = g_callable_info_get_n_args ((GICallableInfo *)info);
  g_assert (argc + 1 >= n_args);
				 
  in_args = g_new0 (GArgument, n_args);
  out_args = g_new0 (GArgument, n_args);
  n_in_args = 0;
  n_out_args = 0;

  /* Here we just assume that the first arg is the this pointer, and
     that it has the right type. That should be OK unless the code
     does weird stuff.
     TODO: We could check the type here to avoid weird errors */
  in_args[n_in_args++].v_pointer = wrapped_object;

  result = TRUE;
  for (i = 0; result && (i < (n_args - 1)); i++)
    {
      arg_info = g_callable_info_get_arg ((GICallableInfo *)info, i+1);

      dir = g_arg_info_get_direction (arg_info);
      type_info = g_arg_info_get_type (arg_info);
      
      if (dir == GI_DIRECTION_IN)
	{
	  script_value = _g_script_value_new_from_jsval (argv[i]);
	  if (_g_script_convert_js_to_arg (script_value,
					   type_info,
					   &in_args[n_in_args],
					   g_arg_info_may_be_null (arg_info),
					   &err_str))
	    {
	      /* converted to in_args */
	      n_in_args ++;
	    }
	  else
	    {
	      throw_error (cx, err_str);
	      result = FALSE;
	    }
	  g_object_unref (script_value);
	}
      else if (dir == GI_DIRECTION_OUT)
	{
	  /* TODO */
	  throw_error (cx, "Out params not supported");
	  result = FALSE;
	}
      else /* IN_OUT */
	{
	  /* TODO */
	  throw_error (cx, "In/Out params not supported");
	  result = FALSE;
	}
    }

  error = NULL;
  if (result)
    {
      if (g_function_info_invoke (info,
				  in_args,
				  n_in_args,
				  out_args,
				  n_out_args,
				  &retval,
				  &error))
	{
	  GScriptValue *grval;
	  GITransfer caller_owns;
	  
	  grval = NULL;
	  type_info = g_callable_info_get_return_type ((GICallableInfo *)info);
	  if (g_type_info_get_tag (type_info) == GI_TYPE_TAG_VOID)
	    {
	      *rval = JSVAL_VOID;
	    }
	  else
	    {
	      if (_g_script_convert_arg_to_js (&retval,
					       type_info,
					       &grval,
					       &err_str))
		{
		  *rval = _g_script_value_get_jsval (grval);
		  g_object_unref (grval);
		}
	      else
		{
		  throw_error (cx, err_str);
		  result = FALSE;
		}

	      caller_owns = g_callable_info_get_caller_owns ((GICallableInfo *)info);
	      if (caller_owns == GI_TRANSFER_CONTAINER)
		{
		  /* Free retval shallow */
		  _g_script_arg_free (&retval, type_info, FALSE);
		}
	      else if (caller_owns == GI_TRANSFER_EVERYTHING)
		{
		  /* Free retval deep */
		  _g_script_arg_free (&retval, type_info, TRUE);
		}
	    }
	}
      else
	{
	  throw_error (cx, error->message);
	  g_error_free (error);
	  result = FALSE;
	}
    }

  for (i = 1; i < n_args; i++)
    {
      GITransfer called_owns;
      
      arg_info = g_callable_info_get_arg ((GICallableInfo *)info, i);
      
      called_owns = g_arg_info_get_ownership_transfer (arg_info);
      type_info = g_arg_info_get_type (arg_info);

      /* TODO: Check out exactly what these means,
	 transfer_container can't really mean the called function owns
	 the contaner but not its contents... really... */
      if (called_owns == GI_TRANSFER_NOTHING)
	_g_script_arg_free (&in_args[i], type_info, TRUE /* deep? */);
      else if (called_owns == GI_TRANSFER_CONTAINER)
	_g_script_arg_free (&in_args[i], type_info, FALSE /* deep? */);
      /* GI_TRANSFER_EVERYTHING -> don't free, called_owns */
	
    }

  g_free (in_args);
  
  /* TODO: Free the out args */
  
  g_free (out_args);
  
  return result;
}

static gboolean
define_property_from_function_info (JSContext *cx,
				    JSObject *obj,
				    GIFunctionInfo *info)
{
  JSFunction *f;
  JSObject *f_obj;
  GIFunctionInfo **infop;
  GScriptValue *symbol;
  jsval v;
  GIFunctionInfoFlags flags;
  int n_args, i;
  GIArgInfo *arg_info;
  GITypeInfo *type_info;
  GIDirection dir;
  const char *symbol_name;
  static const char *hidden_symbols[] = {
    "g_object_ref",
    "g_object_ref_sink",
    "g_object_unref",
    "gtk_object_ref",
    "gtk_object_unref",
    "gtk_object_weakref",
    "gtk_object_weakunref",
    "gtk_object_sink",
    "gtk_object_set",
    "gtk_object_get",
    "gtk_object_set_data",
    "gtk_object_set_data_full",
    "gtk_object_remove_data",
    "gtk_object_get_data",
    "gtk_object_remove_no_notify",
    "gtk_object_set_user_data",
    "gtk_object_get_user_data",
    "gtk_object_set_data_by_id",
    "gtk_object_set_data_by_id_full",
    "gtk_object_get_data_by_id",
    "gtk_object_remove_data_by_id",
    "gtk_object_remove_no_notify_by_id",
    "gtk_object_add_arg_type",
    "gtk_widget_ref",
    "gtk_widget_unref",
    "gtk_widget_set",
    NULL
  };

  /* Check if we should allow this and if not return FALSE */

  if (g_base_info_is_deprecated ((GIBaseInfo *)info))
    return FALSE;

  flags = g_function_info_get_flags (info);

  if ((flags & GI_FUNCTION_IS_CONSTRUCTOR) ||
      (flags & GI_FUNCTION_IS_GETTER) ||
      (flags & GI_FUNCTION_IS_SETTER))
    return FALSE;

  n_args = g_callable_info_get_n_args ((GICallableInfo *)info);

  for (i = 1; i < n_args; i++)
    {
      arg_info = g_callable_info_get_arg ((GICallableInfo *)info, i);

      dir = g_arg_info_get_direction (arg_info);
      if (dir == GI_DIRECTION_OUT ||
	  dir == GI_DIRECTION_INOUT)
	{
	  //g_print ("hiding %s (non-in)\n", g_base_info_get_name ((GIBaseInfo *)info));
	  return FALSE; /* non-in args not yet supported */
	}
	  
      type_info = g_arg_info_get_type (arg_info);

      if (!_g_script_convert_type_supported (type_info))
	{
	  //g_print ("hiding %s (arg type) %s\n", g_base_info_get_name ((GIBaseInfo *)info), g_function_info_get_symbol (info));
	  return FALSE;
	}
    }

  symbol_name = g_function_info_get_symbol (info);
  for (i = 0; hidden_symbols[i] != NULL; i++)
    {
      if (strcmp (symbol_name, hidden_symbols[i]) == 0)
	return FALSE;
    }
  
  f = JS_DefineFunction (cx, obj, g_base_info_get_name ((GIBaseInfo *)info),
			 invoke_method,
			 g_callable_info_get_n_args ((GICallableInfo *)info) - 1,
			 JSPROP_ENUMERATE);

  infop = g_new (GIFunctionInfo *, 1);
  g_assert (((gsize)infop) % 2 == 0);
  *infop = info;

  f_obj = JS_GetFunctionObject (f);

  symbol = g_script_value_new_from_string (symbol_name);
  v = _g_script_value_get_jsval (symbol);
  JS_SetProperty (cx, f_obj, "symbol", &v);
  g_object_unref (symbol);
  
  if (!JS_SetReservedSlot (cx, f_obj, 0, PRIVATE_TO_JSVAL (infop)))
    g_warning ("Unable to set reserved slot of function");
  
  return TRUE;
  
}


static JSBool
lazy_enumerate_object_prototype (JSContext *cx, JSObject *obj)
{
  JSClass *class;
  GType type;
  GParamSpec **param_specs;
  GObjectClass *oclass;
  guint n, i;
  gpointer private;
  GIBaseInfo *info;

  private =  JS_GetInstancePrivate (cx, obj, &gobject_prototype_class, NULL);
  if (private == NULL)
    return JS_TRUE;

  class = JS_GetClass (obj);
  type = (GType)private;
  oclass = g_type_class_ref (type);
  
  param_specs = g_object_class_list_properties (oclass, &n);
  if (param_specs != NULL)
    {
      for (i = 0; i < n; i++)
	define_property_from_paramspec (cx, obj, param_specs[i]);
    }
  
  g_type_class_unref (oclass);

  info = g_irepository_find_by_gtype (g_irepository_get_default (),
				      type);
  if (info != NULL)
    {
      GIFunctionInfo *function;
      int n_methods;
      
      g_assert (g_base_info_get_type (info) == GI_INFO_TYPE_OBJECT);
      
      n_methods = g_object_info_get_n_methods ((GIObjectInfo *)info);
      for (i = 0; i < n_methods; i++)
	{
	  function = g_object_info_get_method ((GIObjectInfo *)info, i);
	  define_property_from_function_info (cx, obj, function);
	}
    }
  
  return JS_TRUE;
}

static JSBool
lazy_enumerate_object (JSContext *cx, JSObject *obj)
{
  GType type;
  GObject *wrapped_object;
  guint n, i;
  guint *signal_ids;
  GSignalQuery query;

  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object == NULL)
    return JS_TRUE;

  /* This is a proxy object, not a prototype.
   * We want to set the properties on the prototypes instead
   * of the object proxies themselves.
   * However, the signal objects go on the object itself, due
   * to the back-reference required for e.g. connect().
   */
  type = G_OBJECT_TYPE (wrapped_object);
  while (type != 0)
    {
      signal_ids = g_signal_list_ids (type, &n);
      for (i = 0; i < n; i++)
	{
	  g_signal_query (signal_ids[i], &query);
	  if (query.signal_id != 0)
	    define_property_for_signal (cx, obj, query.signal_name, signal_ids[i]);
	}
      g_free (signal_ids);
      type = g_type_parent (type);
    }
  
  return JS_TRUE;
}

static JSBool
lazy_resolve_object_prototype (JSContext *cx, JSObject *obj, jsval id,  uintN flags, JSObject **objp)
{
  gpointer private;
  char *propname;
  GParamSpec *param_spec;
  GObjectClass *oclass;
  guint sig_id;
  GType type;
  GIBaseInfo *info;

  private = JS_GetInstancePrivate (cx, obj, &gobject_prototype_class, NULL);
  if (private == NULL)
    return JS_TRUE;
  g_assert (JSVAL_IS_STRING (id));

  type = (GType)private;

  /* Look at properties */
  
  propname = JS_GetStringBytes (JSVAL_TO_STRING (id));

  oclass = g_type_class_ref (type);
  
  param_spec = g_object_class_find_property (oclass, propname);

  if (param_spec != NULL)
    {
      define_property_from_paramspec (cx, obj, param_spec);
      g_type_class_unref (oclass);
      *objp = obj; /* Signal success */
      return JS_TRUE;
    }
  
  g_type_class_unref (oclass);

  /* Look at signals */

  sig_id = 0;
  if (propname[0] == 'o' && propname[1] == 'n' && propname[2] == '_')
    sig_id = g_signal_lookup (propname+3, type);

  if (sig_id != 0)
    {
      /* *objp here is the original object, not the prototype */
      define_property_for_signal (cx, *objp, propname+3, sig_id);
      /* No need to set *objp, as its already right */
      return JS_TRUE;
    }

  /* Look at methods (via GObject Introspection) */

  info = g_irepository_find_by_gtype (g_irepository_get_default (),
				      type);
  if (info != NULL)
    {
      GIFunctionInfo *function;
      
      g_assert (g_base_info_get_type (info) == GI_INFO_TYPE_OBJECT);
      
      function = g_object_info_find_method ((GIObjectInfo *)info, propname);

      if (function &&
	  define_property_from_function_info (cx, obj, function))
	{
	  *objp = obj; /* Signal success */
	  return JS_TRUE;
	}
    }
  
  *objp = NULL; /* Signal no such property */
  return JS_TRUE;
}


static JSBool
lazy_resolve_object (JSContext *cx, JSObject *obj, jsval id,  uintN flags, JSObject **objp)
{
  GObject *wrapped_object;

  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object == NULL)
    {
      *objp = NULL; /* Signal no such property */
      return JS_TRUE;
    }
  
  /* This is a proxy object, not a prototype.
     We want to set the properties on the prototypes instead
     of the object proxies themselves, so just return */
  
  *objp = NULL; /* Signal no such property */
  
  return JS_TRUE;
}

static JSBool
gobject_to_string (JSContext *cx,
		   JSObject *obj,
		   uintN argc,
		   jsval *argv,
		   jsval *rval)
{
  GObject *wrapped_object;
  gpointer private;
  GType type;
  char *str;
  GScriptValue *v;

  str = NULL;
  
  wrapped_object =  JS_GetInstancePrivate (cx, obj, &gobject_class, NULL);
  if (wrapped_object != NULL)
    {
      str = g_strdup_printf ("[%s %p]",
			     g_type_name (G_OBJECT_TYPE (wrapped_object)),
			     wrapped_object);
    }
  else
    {
      private =  JS_GetInstancePrivate (cx, obj, &gobject_prototype_class, NULL);
      if (private != NULL)
	{
	  type = (GType)private;
	  str = g_strdup_printf ("[class %s]",
				 g_type_name (type));
	}
    }

  if (str != NULL)
    {
      v = g_script_value_new_from_string (str);
      *rval = _g_script_value_get_jsval (v);
      g_object_unref (v);
      g_free (str);
    }
  else
    *rval = JSVAL_VOID;
  
  return TRUE;
}

static JSFunctionSpec gobject_methods[] = {
  {"toString",  gobject_to_string,             0,JSPROP_ENUMERATE, 0},
  {NULL,NULL,0,0,0}
};


static JSObject * /* value always rooted by the GType */
ensure_class_for_gtype (GType type)
{
  GType parent;
  JSContext *cx;
  JSObject *gtype_object;
  JSObject *parent_proto;
  static GQuark qname = 0;
  JSObject **qdata;
  const char *old_name;

  if (qname == 0)
    qname = g_quark_from_static_string ("js-defined-type");

  if ((qdata = g_type_get_qdata (type, qname)) != NULL)
    {
      /* We've already created this class... */
      return *qdata;
    }
  
  cx = _g_script_get_internal_context ();
  gtype_object = _g_script_get_gtype_object ();

  qdata = g_new0 (JSObject *, 1);
  if (!JS_AddRoot (cx, qdata))
    {
      g_warning ("failed to root type qdata");
      g_free (qdata);
      return NULL;
    }

  parent_proto = NULL;
  parent = g_type_parent (type);
  if (parent != 0)
    {
      parent_proto = ensure_class_for_gtype (parent);
      if (parent_proto == NULL)
	{
	  g_warning ("failed to root type qdata");
	  JS_RemoveRoot (cx, qdata);
	  g_free (qdata);
	  return NULL;
	}
    }

  /* Temporarily set the class name so we create a constructor
     with the right name. Then change back to the old one. This is
     not threadsafe, but then we're not using the threadsafe
     spidermonkey stuff either. */
  old_name = gobject_prototype_class.name;
  gobject_prototype_class.name = g_type_name (type);
  *qdata = JS_InitClass (cx, gtype_object, parent_proto,
			 &gobject_prototype_class,
			 construct_object, 0 /* TODO: uintN nargs for constructor */,
			 NULL, NULL, //JSPropertySpec *ps, JSFunctionSpec *fs,
			 NULL, NULL //JSPropertySpec *static_ps, JSFunctionSpec *static_fs
			 );

  if (type == G_TYPE_OBJECT)
    JS_DefineFunctions (cx, *qdata, gobject_methods);      
  
  gobject_prototype_class.name = old_name;
  

  if (*qdata)
    JS_SetPrivate (cx, *qdata, (gpointer)type);

  g_type_set_qdata (type, qname, qdata);

  return *qdata;
}

static void
object_toggle_notify_cb (gpointer      data,
			 GObject      *object,
			 gboolean      is_last_ref)
{
  JSObject **user_data;
  JSContext *cx;
  
  user_data = g_object_get_data (object, "js-wrapper");

  //g_print ("toggle notify is_last_ref=%d for wrapped object %p (proxy %p)\n", is_last_ref, object, *user_data);

  
  /* TODO: How do we know the context is still around?? (and not in use elsewhere?) */
  cx = data;
  
  if (is_last_ref)
    {
      /* By now the only ref to the wrapped object is the js proxy.
       * We drop the root for that to allow it to be collected when
       * the last ref to the proxy goes away on the JS side.
       * When this happens we free the wrapped object.
       */
      JS_RemoveRoot (cx, user_data);
    }
  else
    {
      /* Oh, but before the above happened the object got another ref, so
       * we root the proxy again to keep it alive for as long as the wrapped
       * object lives, to ensure we keep around any important data stored
       * on the proxy object
       */
      JS_AddRoot (cx, user_data);
    }

}

gboolean
wrap_object (GObject *object,
	     JSObject **res /* Should be rooted */
	     )
{
  GType type;
  JSObject *class_prototype;
  JSObject **user_data;
  JSContext *cx;

  /* check if already wrapped */
  user_data = g_object_get_data (object, "js-wrapper");
  if (user_data != NULL)
    {
      *res = *user_data;
      return TRUE;
    }

  cx = _g_script_get_internal_context ();
  
  type = G_OBJECT_TYPE (object);

  class_prototype = ensure_class_for_gtype (type);
  
  /* No need for gc roots, as the class_property lives with the gtype */
  if (class_prototype == NULL)
    return FALSE;

  user_data = g_new0 (JSObject *, 1);
  
  if (!JS_AddRoot (cx, user_data))
    {
      g_free (user_data);
      return FALSE;
    }

  g_object_set_data_full (object,
			  "js-wrapper",
			  user_data, g_free);
  
  *user_data = JS_NewObject (cx, &gobject_class, class_prototype, NULL);
  
  if (*user_data == NULL ||
      !JS_SetPrivate (cx, *user_data, object))
    {
      g_object_set_data (object,
			 "js-wrapper",
			 NULL);
      JS_RemoveRoot (cx, user_data);
      g_free (user_data);
      return FALSE;
    }

  /* We succeeded in wrapping the object, now ref it with a toggle ref */
  
  g_object_add_toggle_ref (object,
			   object_toggle_notify_cb,
			   cx /* TODO: How to ensure lifetime of the cx object? */
			   ); 

  /* We now have a strong ref in both ways, and both objects
     point to each other. In the toggle ref callback we drop the
     GC root to the proxy object if there is only the toggle ref
     to ensure we can collect the two objects */

  *res = *user_data;
  return TRUE;
}
