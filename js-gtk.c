/* Compile with:
gcc -g -I . -I ./Linux_All_DBG.OBJ `pkg-config --libs --cflags gtk+-2.0 gobject-introspection-1.0` -DXP_UNIX -O -Wall js-gtk.c Linux_All_DBG.OBJ/libjs.a -o js-gtk
*/
#include <girepository.h>
#include "gscript/gscriptengine.h"
#include <gtk/gtk.h>
#include <glib.h>
#include <string.h>

GScriptEngine *engine;
GtkTextBuffer *text_buffer;
GtkWidget *textview;

static void
print (const char *s)
{
  GtkTextIter iter;
  GtkTextMark *mark;
  
  gtk_text_buffer_get_end_iter (text_buffer, &iter);
  gtk_text_buffer_insert (text_buffer, &iter, s, -1);
  gtk_text_buffer_get_end_iter (text_buffer, &iter);
  gtk_text_buffer_insert (text_buffer, &iter, "\n", -1);

  mark = gtk_text_buffer_get_insert  (text_buffer);
  gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (textview), mark, 0.0, FALSE, 0.0, 0.0);
}

static void
activate_entry (GtkEntry *entry)
{
  GScriptValue *res;
  const char *text;
  char *s;

  text = gtk_entry_get_text (entry);

  s = g_strdup_printf ("%s :", text);
  print (s);
  g_free (s);
  res = g_script_engine_evaluate_script (engine, text);
  s = g_strdup_printf ("-> %s", g_script_value_to_string (res));
  print (s);
  g_object_unref (res);
  g_free (s);

  gtk_entry_set_text (entry, "");
}

static void
setup_shell (void)
{
  GtkWidget *window, *vbox, *entry, *sw;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  vbox = gtk_vbox_new (FALSE, 1);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_size_request (sw, 400, 600);
  gtk_box_pack_start (GTK_BOX (vbox),
		      sw,
		      TRUE, TRUE, 0);  

  textview = gtk_text_view_new ();
  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
  gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (textview), FALSE);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);

  gtk_container_add (GTK_CONTAINER (sw), textview);
  
  entry = gtk_entry_new ();
  g_signal_connect (entry, "activate", G_CALLBACK (activate_entry), NULL);
  
  gtk_box_pack_start (GTK_BOX (vbox),
		      entry,
		      FALSE, FALSE, 0);  

  gtk_widget_show_all (window);
  gtk_widget_grab_focus (entry);  
}


static GScriptValue *
print_cb (int n_args, GScriptValue **args)
{
  char *s;

  s = g_script_value_to_string (args[0]);
  if (s != NULL)
    {
      print (s);
      g_free (s);
    }
  
  return NULL;
}

static gboolean
idle_cb (gpointer data)
{
  GScriptValue *arg = G_SCRIPT_VALUE (data);

  g_script_engine_call (engine,
			NULL,
			arg, 0, NULL);
  
  g_object_unref (arg);
  return FALSE;
}

static GScriptValue *
add_idle (int n_args, GScriptValue **args)
{
  guint tag;

  tag = g_idle_add (idle_cb, g_object_ref (args[0]));

  return g_script_value_new_from_uint32 (tag);
}

int
main(int argc, char *argv[])
{
  GScriptValue *wrapped, *f;
  GScriptValue *res;
  GScriptValue *global;
  GtkWidget *label;
  GError *error;
  
  gtk_init(&argc, &argv);

  error = NULL;
  if (!g_irepository_require (g_irepository_get_default (),
			      "GObject", 0, &error))
    {
      g_print ("Unable to load GObject introspection data: %s\n",
	       error->message);
      return 1;
    }
  
  error = NULL;
  if (!g_irepository_require (g_irepository_get_default (),
			      "Gtk", 0, &error))
    {
      g_print ("Unable to load Gtk introspection data: %s\n",
	       error->message);
      return 1;
    }

  engine = g_script_engine_new ();

  label = gtk_label_new ("Text of label");
  g_object_ref_sink (label);
  
  wrapped = g_script_value_new_from_gobject (G_OBJECT (label));
  global = g_script_engine_get_global (engine);
  g_script_value_set_property (global,
			       "label",
			       wrapped);
  
  g_object_unref (label); /* Now label is only owned by the proxy object */
  
  setup_shell ();

  wrapped = g_script_value_new_from_gobject (G_OBJECT (textview));
  global = g_script_engine_get_global (engine);
  g_script_value_set_property (global,
			       "tv",
			       wrapped);

  f = g_script_value_new_from_function (print_cb, 1);
  g_script_value_set_property (global,
			       "print",
			       f);
  g_object_unref (f);
  
  f = g_script_value_new_from_function (add_idle, 1);
  g_script_value_set_property (global,
			       "add_idle",
			       f);
  g_object_unref (f);
  
  if (0)
    {
      print ("Running test script");
      print ("---------------------");
      
      
      res = g_script_engine_evaluate_script (engine,
					     "print (label.on_move_cursor);"
					     "print (label.on_move_cursor.connect);"
					     "label.on_move_cursor.connect( function (x, y, z) {print (this);print(x);print(y);print(z)});"
					     "print (label.label);"
					     "print (label.style);"
					     "print (label.wrap);"
					     "print (label.nonexist);"
					     "add_idle (function () { print ('in idle'); });"
					     );
      
      print ("---------------------");
      print ("script result:");
      print (g_script_value_to_string (res));
      
      g_signal_emit_by_name (label, "move_cursor", 0, 0, FALSE);
    }

  {
    gchar *contents;
    if (g_file_get_contents ("gtk.js",
			     &contents, NULL, NULL))
      {
	print ("Running gtk.js");
	print ("---------------------");
      
      
	res = g_script_engine_evaluate_script (engine, contents);
      
      print ("---------------------");
      print ("script result:");
      print (g_script_value_to_string (res));
      print ("---------------------");
    }
  }
  
  
  gtk_main ();
  
  return 0;
}
